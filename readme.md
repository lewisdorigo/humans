# Humans.txt

Adds a humans.txt file, allowing you to credit the people who worked on the project.

## Usage

In your theme, you can add a `humans.txt` file. This will be parsed by the plugin, allowing some things to be dynamically added.

### Special Tags

* `{{team}}` Display the team information.
* `{{modified_date}}` Parses the date the site was last modified, both from posts, and from file modified times.
* `{{language}}` Gets the site language, as set in WordPress

### Filters

* `Brave/Humans/Team/PostType` can be used to modify the post type used for the site’s team members.
* `Brave/Humans/Team/Member` can be used to modify the data for each team member. Returns an array of arrays, containing each row of the team member’s data. Each array should have a `title` and a `content` key.
* `Brave/Humans/Team/Return` can be used to modify the full team members object.

### Example

MadeBrave’s `humans.txt` file looks like this:

```

                                                                ;C@
                                                                @@@
                                                               0@@:
                 t@@G                                         L@@1     ;@@@@@.
              @@@@@@@@8   ,C1   8@@@@@                       i@@G    L@@8  @@8
 L         L@@@0    @@@ @@@@@@i@; G@@t                 @@@@@@@@@    @@@,  ,@@
@@      ,@@@0      @@@C@  0@@@C  ;@@G    1@C         @@@t  C@@@    G@@i   @@
f@@Lf@@@@0        0@@@1  1@@@    @@@  1@@@@@@0      @@@    @@@    f@@L .@@0  .8
  8@@G           f@@@   ,@@@    @@@  @@@   ;@@@    8@@:   @@@    ;@@@@@C    8@.
                ;@@@    @@@    @@@  @@@    @@@    L@@t   8@@:   G@@@      f@G
               .@@@    @@@    @@@. 8@@,   @@@    t@@0   0@@t   @@@@@   ,@@G
               @@@    @@@    G@@; C@@1   @@@,   @@@@   @@@8  @@. :@@@@@G
              @@@    8@@:   t@@f 1@@C   @@@1  i@@@@  8@.@@@@@
             @@@,   L@@t   :@@0 ,@@@   @@@@ C@@ i@@@8
            0@@i   i@@G    @@@  8@@80@8 t@@@;                                        8@@@@@G
           f@@C   .@@@    @@@    i0L                                         0@@   8@@L  @@@
          ;@@8    @@@    @@@     :11                                  @@@   0@@   @@@.  ,@@
         .@@@           @@@,  L@@@@@@@.   8@@            G@@@@@.     L@@C  0@@   0@@i  :@@
                       C@@i  @@@   :@@G   @@@          f@@8  t@@@    @@@  0@@   f@@G;8@@i  i@;
                      i@@C  @@@    @@@     @@@@:      C@@1   @@@.   0@@t 0@@   i@@0fi     @@
                      @@@  @@@    @@@     @@ @@@f    1@@C   C@@i    @@@ 0@@@.t@@@@      8@1
                      i@@@@@@.G@@@@.     @@  @@@    ,@@@   1@@L   :@@@;0@@ ;C. @@@0.,8@@:
                        ,@@@@@@@@@@8    @@  0@@;    @@@   i@@8   @@@@@8@@       t@@@8
                        t@@t     @@@@  @0  f@@f   L@@@   0@@@  G@L.@@@@L              1@
                  @@@@@@@@@@@,    @@@C@f  ;@@0   @@@@; L@L@@@@@i                     @@
                @@@8   @@@.G@@@@@f@@@@;   @@@ .@@.:@@@@:                           @@C
               8@@    @@@      .@@@@@     i@@@0                                 1@@8
               @@@   @@@         @@@@@@@@G                                   G@@@t
               ;@@8 @@@        C@@0     f@@@@@@@L                       f@@@@@1
                 @@@@@,    .G@@@@            .f@@@@@@@@@@@@@@@@@@@@@@@@@@@;
                   .0@@@@@@@@@:                       .;tC0@@@@@0Li.


/* TEAM */
	{{team}}

/* SITE */
	Last update: {{modified_date}}
	Language: {{language}}
	Doctype: HTML5
	Standards: HTML5, CSS3, JS, PHP
	Components: Wordpress, jQuery, picturefill, Owl Carousel
	Software: Coda, Sketch, Photoshop, Transmit
```

The compiled MadeBrave `humans.txt` file is available to view at https://madebrave.com/humans.txt.


## Including in a Composer Project

```
{
    "name": "exampleProject",
    "repositories": [
        {
            "type": "git",
            "url": "ssh://git@bitbucket.org/madebrave/humans.git"
        }
    ],
    "require": {
        "madebrave/humans" : "^1.0.1",
    },
    "extra": {
        "installer-paths": {
            "www/wp-content/mu-plugins/{$name}/": [
                "type:wordpress-muplugin"
            ]
        }
    }
}
```