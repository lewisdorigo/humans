<?php
/**
 * Plugin Name:       Humans.txt
 * Plugin URI:        https://bitbucket.org/lewisdorigo/humans
 * Description:       Adds Humans.txt
 * Version:           2.0.1
 * Author:            Lewis Dorigo
 * Author URI:        https://dorigo.co/
 */

namespace Dorigo\Humans;

function init() {
	global $wp_rewrite;

	$rewrite_rules = get_option('rewrite_rules');

	add_filter('query_vars', function($qv) {
    	$qv[] = "humans";
    	return $qv;
    });

	add_rewrite_rule('humans\.txt$', $wp_rewrite->index . '?humans=1', 'top');

	// register author link tag action if enabled
	add_action('wp_head', __NAMESPACE__.'\\author_tag', 1);

	// flush rewrite rules if ours is missing
	if (!isset($rewrite_rules['humans\.txt$'])) {
		flush_rewrite_rules(false);
	}
}
add_action('init', __NAMESPACE__.'\\init');

function is_humans() {
	return (bool) get_query_var('humans');
}

function get_humans_template_directory() {
    if(file_exists(get_stylesheet_directory().DIRECTORY_SEPARATOR.'humans.txt')) {
    	return get_stylesheet_directory().DIRECTORY_SEPARATOR.'humans.txt';
	} elseif (file_exists(get_template_directory().DIRECTORY_SEPARATOR.'humans.txt')) {
    	return get_template_directory().DIRECTORY_SEPARATOR.'humans.txt';
	} else {
    	return false;
    }
}


function author_tag() {
    if(get_humans_template_directory()) {
        echo '<link rel="author" type="text/plain" href="'.home_url('humans.txt').'">';
    }
}

function template_redirect() {
	if(\Dorigo\Humans\is_humans()) {
		do_action('do_humans');
		exit;
	}
}
add_action('template_redirect', __NAMESPACE__.'\\template_redirect', 8);

function get_team() {

    $team = get_posts(array(
        'post_type'         => apply_filters('Dorigo/Humans/Team/PostType','team-member'),
        'posts_per_page'    => -1,
        'post_status'       => 'publish',
        'orderby'           => 'menu_order title',
        'order'             => 'asc',
    ));

    $return = [];

    foreach($team as $person) {

        $row = [
            [
                'title' => '',
                'content' => apply_filters('the_title',$person->post_title),
            ],
        ];

        $return[$person->ID] = apply_filters('Dorigo/Humans/Team/Member', $row, $person, $team);
    }

    $return = apply_filters('Dorigo/Humans/Team/Return', $return, $team);

    $returnString = '';

    $i = 0;

    foreach($return as $person) {
        foreach($person as $row) {
            $returnString .= ($i!==0?PHP_EOL."\t":'').($row['title'] ? "{$row['title']}: " : '').$row['content'];
            $i++;
        }

        $returnString .= PHP_EOL;
    }

    return $returnString;
}

function do_humans() {
    $file = get_humans_template_directory();

	if(!$file) {
        status_header( 404 );
        nocache_headers();
        include(get_query_template('404'));
        die();
	}

	header('Content-Type: text/plain; charset=utf-8');

	/** GET TEMPLATE MODIFIED DATE **/
	$stylesheet_directory_modified  = filemtime(get_stylesheet_directory());
	$template_directory_modified    = filemtime(get_template_directory());


	/** GET POST MODIFIED DATE **/
	$posts = get_posts(array(
    	'post_type'     => 'any',
    	'post_status'   => 'publish',
    	'orderby'       => 'modified',
    	'order'         => 'desc',
    	'posts_per_page'=> 1
	));

	$post = (isset($posts[0]) ? $posts[0] : false);
	$post_modified_date = ($post ? strtotime($post->post_modified) : 0);



    /** WHICH IS MOST RECENT **/
	$last_modified = max($stylesheet_directory_modified, $template_directory_modified, $post_modified_date);


    /** GET SITE LANGUAGE **/
	$lang = get_site_option( 'WPLANG' );
	require_once( ABSPATH . 'wp-admin/includes/translation-install.php' );
	$translations = wp_get_available_translations();
    $site_language = (isset($translations[$lang]) ? $translations[$lang]['native_name'] : 'English');




    /** UPDATE THE TEMPLATE THEN EXIT **/
	$template = file_get_contents($file);

	echo str_replace(array(
    	'{{team}}',
	    '{{modified_date}}',
	    '{{language}}'
	), array(
    	\Dorigo\Humans\get_team(),
	    date('Y/m/d',$last_modified),
	    $site_language
	), $template);
}
add_action('do_humans', __NAMESPACE__.'\\do_humans');
